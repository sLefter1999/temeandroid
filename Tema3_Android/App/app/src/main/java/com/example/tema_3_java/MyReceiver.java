package com.example.tema_3_java;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Objects;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Objects.requireNonNull(intent.getAction()).equalsIgnoreCase("android.intent.action.MAIN")){
            Toast.makeText(context, "Alarm set", Toast.LENGTH_LONG).show();

        }
    }
}
