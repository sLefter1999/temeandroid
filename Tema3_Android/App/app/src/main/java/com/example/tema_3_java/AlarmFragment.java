package com.example.tema_3_java;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.provider.AlarmClock;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;


public class AlarmFragment extends Fragment {

    TextView dateTv;
    TextView timeTv;
    Button chooseTimeBtn;
    Button chooseDateBtn;
    Button setAlarmBtn;
    Calendar dateTime;
    int year = 0, month = 0, day = 0;
    int hour = 0, minute = 0;

    public AlarmFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alarm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dateTv = view.findViewById(R.id.tv_date);
        timeTv = view.findViewById(R.id.tv_time);
        chooseDateBtn = view.findViewById(R.id.btn_choose_date);
        chooseTimeBtn = view.findViewById(R.id.btn_choose_time);
        setAlarmBtn = view.findViewById(R.id.btn_set_alarm);
        dateTime = Calendar.getInstance();

        chooseDateBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                chooseDate();
            }
        });

        chooseTimeBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                chooseTime();
            }
        });

        setAlarmBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                setAlarm();
            }
        });
    }

    private void chooseDate()
    {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DATE);

        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder
                            .append(dayOfMonth).append("/")
                            .append(month+1).append("/")
                            .append(year);

                    dateTv.setText(stringBuilder.toString());

                    dateTime.set(Calendar.YEAR, year);
                    dateTime.set(Calendar.MONTH, month);
                    dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            }
        }, year, month, day);

        datePickerDialog.show();
    }

    private void chooseTime()
    {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        Boolean is24HourFormat = DateFormat.is24HourFormat(getContext());

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder
                        .append(hourOfDay).append(":")
                        .append(minute);

                timeTv.setText(stringBuilder.toString());

                dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                dateTime.set(Calendar.MINUTE, minute);
            }
        }, hour, minute, true);

        timePickerDialog.show();
    }

    private  void setAlarm()
    {

//        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
//        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "My alarm!");
//        intent.putExtra(AlarmClock.EXTRA_MINUTES, dateTime.get(Calendar.MINUTE));
//        intent.putExtra(AlarmClock.EXTRA_HOUR, dateTime.get(Calendar.HOUR_OF_DAY));
//        startActivity(intent);




        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(getContext(), MyReceiver.class);
        intent.setAction("android.intent.action.MAIN");

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent,0);

        if( alarmManager != null)
            alarmManager.set(AlarmManager.RTC_WAKEUP, dateTime.getTimeInMillis(), pendingIntent);

        Toast.makeText(getContext(), dateTime.getTime().toString(), Toast.LENGTH_LONG).show();

    }

}
