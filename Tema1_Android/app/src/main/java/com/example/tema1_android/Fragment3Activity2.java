package com.example.tema1_android;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class Fragment3Activity2 extends Fragment
{
    Button btn_back;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment3activity2_layout,container,false);

        btn_back = (Button)view.findViewById(R.id.btn_BACK);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = Activity2.fragmentManager.beginTransaction();

                fragmentTransaction.remove(Activity2.fragmentManager.findFragmentById(Activity2.fragmentManager.getFragments().get(Activity2.fragmentManager.getFragments().size()-1).getId()));

                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
