package com.example.tema1_android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class Fragment1Activity2 extends Fragment
{
    Button button;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1activity2_layout,container,false);

        button =(Button)view.findViewById(R.id.button_AddF2A2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = Activity2.fragmentManager.beginTransaction();

                Fragment2Activity2 fragment2Activity2 = new Fragment2Activity2();

                fragmentTransaction.add(R.id.fragment_container,fragment2Activity2,null);

                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
