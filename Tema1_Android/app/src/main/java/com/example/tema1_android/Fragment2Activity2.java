package com.example.tema1_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.tema1_android.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public class Fragment2Activity2 extends Fragment {

    Button button_goToF3A2;

    Button button_removeF1A2;

    Button button_closeActivity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment2activity2_layout,container,false);

        button_goToF3A2 =(Button)view.findViewById(R.id.btn_GoToFragment3Activity2);

        button_removeF1A2 =(Button)view.findViewById(R.id.btn_RemoveFragment1Activity2);

        button_closeActivity =(Button)view.findViewById(R.id.btn_CloseActivity1);

        button_goToF3A2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = Activity2.fragmentManager.beginTransaction();

                Fragment3Activity2 fragment3Activity2 = new Fragment3Activity2();

                fragmentTransaction.add(R.id.fragment_container,fragment3Activity2,null);

                fragmentTransaction.commit();
            }
        });

        button_removeF1A2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = Activity2.fragmentManager.beginTransaction();

                Fragment fragment = Activity2.fragmentManager.findFragmentByTag("tag");

                if(fragment!=null)
                {
                        fragmentTransaction.remove(fragment);

                        fragmentTransaction.commit();

                    Toast toast = Toast.makeText(getContext(),
                            String.valueOf("F1A2 has been removed"),
                            Toast.LENGTH_SHORT);

                    toast.show();
                }

                else
                {
                    Toast toast = Toast.makeText(getContext(),
                            String.valueOf("F1A2 is already removed"),
                            Toast.LENGTH_SHORT);

                    toast.show();
                }
            }
        });

        button_closeActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return view;
    }
}
