package com.example.tema1_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.content.Intent;
import android.os.Bundle;


public class Activity2 extends AppCompatActivity
{
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_layout);

        fragmentManager = getSupportFragmentManager();

        if(findViewById(R.id.fragment_container)!=null)
        {
            if(savedInstanceState!=null)
                return;

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Fragment1Activity2 fragment1Activity2 = new Fragment1Activity2();

            fragmentTransaction.add(R.id.fragment_container,fragment1Activity2,"tag");

            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
