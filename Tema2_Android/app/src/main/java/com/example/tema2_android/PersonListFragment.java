package com.example.tema2_android;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonListFragment extends Fragment {

    private static RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    public static RecyclerAdapter adapter;

    public PersonListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_person_list, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);

        layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);

        try {
            adapter = new RecyclerAdapter(getContext(),new GetAllUsersTask().execute().get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);

        return view;
    }

    public class GetAllUsersTask extends AsyncTask<Void,Void, List<User>>
    {
        @Override
        protected List<User> doInBackground(Void... voids) {
            List<User> userList = AppDatabase.getInstance(getContext()).userDao().getAll();

            return userList;
        }
    }
}
