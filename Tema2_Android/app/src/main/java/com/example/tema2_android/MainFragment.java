package com.example.tema2_android;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private Button addPersonBtn,viewPersonListBtn,removePersonBtn,syncWithServerBtn;
    private EditText firstNameEditText,lastNameEditText;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_main, container, false);

        firstNameEditText = view.findViewById(R.id.firstNameEditText);
        lastNameEditText = view.findViewById(R.id.lastNameEditText);

        initAddPersonBtn(view);
        initViewPersonListBtn(view);
        initRemovePersonBtn(view);
        initSyncWithServerBtn(view);

        return view;
    }


    private void initSyncWithServerBtn(View view) {
        syncWithServerBtn = view.findViewById(R.id.syncWithServerBtn);

        syncWithServerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SyncWithServerTask().execute();
            }
        });
    }

/*    private void handleRespone(String response) throws JSONException {
        JSONArray jsonArray = new JSONArray(response);

        for(int index=0;index<jsonArray.length();index++)
        {
            JSONObject person = null;
            if(jsonArray.get(index) instanceof JSONObject)
            {
                person = (JSONObject)jsonArray.get(index);

                String name = person.getString("name");

                StringTokenizer tokenizer = new StringTokenizer(name," ");

                String firstName = tokenizer.nextToken();
                String lastName = tokenizer.nextToken();

                User user = new User();
                user.firstName = firstName;
                user.lastName = lastName;

                AppDatabase appDatabase = AppDatabase.getInstance(getContext());

                appDatabase.userDao().insert(user);
            }
        }
    }*/

    private void initAddPersonBtn(View view)
    {
        addPersonBtn = view.findViewById(R.id.addUserBtn);

        addPersonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = firstNameEditText.getText().toString();
                String lastName = lastNameEditText.getText().toString();

                if(!firstName.equals("") && !lastName.equals(""))
                {
                    User user = new User();
                    user.firstName = firstName;
                    user.lastName = lastName;

                    new InsertUserTask().execute(user);

                    Toast.makeText(getActivity(),"Person added successfully",Toast.LENGTH_SHORT).show();
                }

                firstNameEditText.setText("");
                lastNameEditText.setText("");
            }
        });
    }

    private void initViewPersonListBtn(View view)
    {
        viewPersonListBtn = view.findViewById(R.id.viewPersonListBtn);

        viewPersonListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fragmentManager = getActivity().getSupportFragmentManager();
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragmentContainer,new PersonListFragment()).addToBackStack("main").commit();
            }
        });
    }

    private void initRemovePersonBtn(View view)
    {
        removePersonBtn = view.findViewById(R.id.removeUserBtn);

        removePersonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = firstNameEditText.getText().toString();
                String lastName = lastNameEditText.getText().toString();

                if(!firstName.equals("") && !lastName.equals(""))
                {
                    User user = new User();
                    user.firstName = firstName;
                    user.lastName = lastName;

                    Boolean result = false;
                    try {
                        result = new ExistUserTask().execute(user).get();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if(result)
                    {
                        new RemoveUserTask().execute(user);
                        Toast.makeText(getActivity(),"Person removed successfully",Toast.LENGTH_SHORT).show();
                    }

                    else Toast.makeText(getActivity(),"User doesn't exits",Toast.LENGTH_SHORT).show();

                    firstNameEditText.setText("");
                    lastNameEditText.setText("");
                }
            }
        });
    }

    public class InsertUserTask extends AsyncTask<User, Void, Void> {
        @Override
        protected Void doInBackground(User ... users) {
            AppDatabase.getInstance(getContext()).userDao().insert(users[0]);
            return null;
        }
    }

    public class SyncWithServerTask extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected Void doInBackground(Void... voids) {
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String url ="https://jsonplaceholder.typicode.com/users";

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        handleRespone(response);
                        Toast.makeText(getContext(),"Succes",Toast.LENGTH_SHORT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error){

                    Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                }
            });

            queue.add(stringRequest);

            return  null;
        }

        private void handleRespone(String response) throws JSONException {
            JSONArray jsonArray = new JSONArray(response);

            for(int index=0;index<jsonArray.length();index++)
            {
                JSONObject person = null;
                if(jsonArray.get(index) instanceof JSONObject)
                {
                    person = (JSONObject)jsonArray.get(index);

                    String name = person.getString("name");

                    StringTokenizer tokenizer = new StringTokenizer(name," ");

                    String firstName = tokenizer.nextToken();
                    String lastName = tokenizer.nextToken();

                    User user = new User();
                    user.firstName = firstName;
                    user.lastName = lastName;

                    AppDatabase appDatabase = AppDatabase.getInstance(getContext());

                    new InsertUserTask().execute(user);
                }
            }
        }
    }

    public class RemoveUserTask extends AsyncTask<User, Void, Void> {
        @Override
        protected Void doInBackground(User ... users) {
            AppDatabase.getInstance(getContext()).userDao().delete(users[0]);
            return null;
        }
    }

    public class ExistUserTask extends AsyncTask<User,Void,Boolean> {
        @Override
        protected Boolean doInBackground(User ... users) {
            User user = AppDatabase.getInstance(getContext()).userDao().getUser(users[0].firstName,users[0].lastName);
            if(user!=null)
                return true;
            return false;
        }
    }

}
