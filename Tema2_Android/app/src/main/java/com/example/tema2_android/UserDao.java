package com.example.tema2_android;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(User user);

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Delete
    void delete(User user);

    @Query("SELECT * FROM user WHERE first_name = :firstName AND last_name =:lastName")
    User getUser(String firstName,String lastName);
}
