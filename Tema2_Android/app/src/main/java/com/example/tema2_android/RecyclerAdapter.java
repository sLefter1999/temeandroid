package com.example.tema2_android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PersonViewHolder>
{

    private Context context;
    private List<User> users;

    RecyclerAdapter(Context context,List<User> users)
    {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        TextView textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.person_layout,parent,false);

        PersonViewHolder personViewHolder = new PersonViewHolder(textView);

        return personViewHolder;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.firstName.setText(users.get(position).firstName + " " + users.get(position).lastName);
    }

    @Override
    public int getItemCount() {


        int number = users.size();
        return  number;
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        public TextView firstName;

        public PersonViewHolder(TextView firstName) {
            super(firstName);

            this.firstName = firstName;
        }
    }

}
